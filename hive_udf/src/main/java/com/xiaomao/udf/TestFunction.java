package com.xiaomao.udf;

import org.apache.hadoop.hive.ql.exec.UDF;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2021/9/24 0024 15:26
 * @description:
 */
public class TestFunction extends UDF {
    public String evaluate(){
        return "test";
    }
}
