package com.xiaomao.udf;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import org.apache.hadoop.hive.ql.exec.UDF;

/**
 * Copyright (C) zhongda
 *
 * @author zx
 * @date 2021/6/21 0021 09:54
 * @description:
 */
public class IDCreatorUDF extends UDF {

    public String evaluate() {
        Long aLong = new DefaultIdentifierGenerator().nextId(new Object());
        return aLong+"";
    }


}
