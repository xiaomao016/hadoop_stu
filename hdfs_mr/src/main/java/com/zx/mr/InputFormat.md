#### InputFomat

——|FileInputFormat

​		——|TextInputFormat ：读取一行，key是偏移量 （采用默认分片机制）

​		——|NLineInputFormat ：读取行，key是偏移量。（按设置的行数 分片）

​		——|KeyValueTextInputFormat：读取一行的第一个字符串（\t标识）作为key，剩余内容作为value  （分片采用默认分片机制	）

​		——|CombineFileInputFormat ：读取行，key是偏移量 （分片采用文件组合方式）

#### Java类型	Hadoop Writable类型
#### 

| **Java类型** | **Hadoop Writable类型** |
| ------------ | ----------------------- |
| boolean      | BooleanWritable         |
| byte         | ByteWritable            |
| int          | IntWritable             |
| float        | FloatWritable           |
| long         | LongWritable            |
| double       | DoubleWritable          |
| String       | Text                    |
| map          | MapWritable             |
| array        | ArrayWritable           |

#### Mapreduce切片机制

